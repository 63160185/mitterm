/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaewalin.midterm1;

/**
 *
 * @author ACER
 */
public class TestSnake {
    public static void main(String[] args) {
        Snake s = new Snake();  //
        s.setName("Brian");
        s.setBreed("Rat Snake");
        s.setColor("White Pink");
        s.setGender("M");
        s.setAge(4);
        
        Snake s2 = new Snake();  //
        s2.setName("Aria");
        s2.setBreed("Milk Snake");
        s2.setColor("Orange White");
        s2.setGender("W");
        s2.setAge(2);
        
        Snake s3 = new Snake();  //
        s3.setName("Evan");
        s3.setBreed("Ball Python");
        s3.setColor("Black White");
        s3.setGender("W");
        s3.setAge(7);
        
        Snake s4 = new Snake();  //
        s4.setName("Jai");
        s4.setBreed("Rainbow Boa");
        s4.setColor("BurntSugar");
        s4.setGender("W");
        s4.setAge(5);
        
        System.out.println("Snake number 1");
        System.out.println("Name : " + s.getName());
        System.out.println("Breed : " + s.getBreed());
        System.out.println("Color : " + s.getColor());
        System.out.println("Gander : " + s.getGender());
        System.out.println("Age : " + s.getAge() +" Year");
        System.out.println("---------------------------------------------------");
        
        System.out.println("Snake number 2");
        System.out.println("Name : " + s2.getName());
        System.out.println("Breed : " + s2.getBreed());
        System.out.println("Color : " + s2.getColor());
        System.out.println("Gander : " + s2.getGender());
        System.out.println("Age : " + s2.getAge() +" Year");
        System.out.println("---------------------------------------------------");
        
         System.out.println("Snake number 3");
        System.out.println("Name : " + s3.getName());
        System.out.println("Breed : " + s3.getBreed());
        System.out.println("Color : " + s3.getColor());
        System.out.println("Gander : " + s3.getGender());
        System.out.println("Age : " + s3.getAge() +" Year");
        System.out.println("---------------------------------------------------");
        
        System.out.println("Snake number 4");
        System.out.println("Name : " + s4.getName());
        System.out.println("Breed : " + s4.getBreed());
        System.out.println("Color : " + s4.getColor());
        System.out.println("Gander : " + s4.getGender());
        System.out.println("Age : " + s4.getAge()+" Year");
        
        System.out.println("---------------------------------------------------");
        
        System.out.println(s);
        System.out.println(s2);
        System.out.println(s3);
        System.out.println(s4);


    }
}
