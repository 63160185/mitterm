/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaewalin.midterm1;

/**
 *
 * @author ACER
 */
//เกริ่น *** Class คือนิยามของ Object เป็นการกำหนดส่วนต่างๆเพื่อที่จะนำไปสร้าง Object
//Object คือ สิ่งที่เกิดขึ้นจริง เขียนโดยโปรแกรม หรือที่เกิดในโลกความของความจริง***
public class Snake {
    //กำหนดค่าตัวแปร 

    String Name; //ให้ Name เป็น String
    String Breed; //ให้ Breed เป็น String
    String Color; // ให้ Color เป็น String
    String Gender; //ให้ gander เป็น string
    int Age; //ให้ age เป็น int

    //กำหนดค่าของ Name โดยการใช้ Setter 
    public void setName(String name) {
        Name = name;
    }

    //กำหนดค่าของ Breed โดยการใช้ Setter
    public void setBreed(String breed) {
        Breed = breed;
    }

    //กำหนดค่าของ Color โดยการใช้ Setter
    public void setColor(String color) {
        Color = color;
    }

    //กำหนดค่าของ Gander โดยการใช้ Setter
    public void setGender(String gender) {
        Gender = gender;
    }

    //กำหนดค่าของ Age โดยการใช้ Setter
    public void setAge(int age) {
        Age = age;
    }

    //เรียกข้อมูล String Name โดยใช้ Getter
    public String getName() {
        return Name;
    }
    //เรียกข้อมูล String Breed โดยใช้ Getter

    public String getBreed() {
        return Breed;
    }
    //เรียกข้อมูล String Coler โดยใช้ Getter

    public String getColor() {
        return Color;
    }

    //เรียกข้อมูล String Gander โดยใช้ Getter
    public String getGender() {
        return Gender;
    }

    //เรียกข้อมูล int Ang โดยใช้ Getter
    public int getAge() {
        return Age;

    }
    @Override
    public String toString() { //โชว์รายละเอียดของงูทั้งหมด return ค่าเป็น string
        return "Snake Name: " + this.Name + ", Snake Breed : " + this.Breed + ", Snake Color : " + this.Color + ", Snake Gender : " + this.Gender + ", Snake Age : " + this.Age +" Year";
    }
    
}
